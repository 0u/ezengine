import ezengine as ez
import chess
import random

def make_move(board: chess.Board, options: dict) -> chess.Move:
    return random.choice(list(board.legal_moves))

engine = ez.from_function(make_move)
engine.serve_uci()
