from distutils.core import setup

setup(name='ezengine',
      version='0.3.2',
      description='Easily create chess engines in python',
      author='Ulisse Mini',
      url='https://gitlab.com/0u/ezengine',
      packages=['ezengine', 'ezengine/lichess_bot'],
      install_requires=[
          'python-chess',
          'requests>=2.21.0',
          'urllib3==1.24.2',
          'backoff==1.7.1',
      ])
