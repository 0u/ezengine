"""
This module provides simple ways to write UCI chess engines,
to learn about the uci protocol visit
http://wbec-ridderkerk.nl/html/UCIProtocol.html
"""

import chess
import sys

# Pure functions used for parsing UCI messages.

def position(args: [str]) -> chess.Board:
    board = chess.Board()

    if args[0] == 'fen':
        fen = ''
        for arg in args[1:]:
            if arg == 'moves': break
            fen += arg + ' '
        board.set_fen(fen)


    if 'moves' in args:
        index = args.index('moves')
        for move in args[index+1:]:
            board.push(board.parse_uci(move))

    return board

def go(args: [str]) -> dict:
    options = {}

    searchmoves = None
    opt_name = '' # will store current opt name (wtime btime etc)
    for i, arg in enumerate(args):
        # if we're currently scanning for an option then do that
        if opt_name != '':
            options[opt_name] = int(arg)
            opt_name = ''

        # bool options (easy)
        elif arg == 'infinite':
            options['infinite'] = True
        elif arg == 'ponder':
            options['ponder'] = True

        # special case (variadic)
        elif arg == 'searchmoves':
            searchmoves = i
            break

        # general case, we are scanning a param name
        else:
            opt_name = arg

    if searchmoves is not None:
        options['searchmoves'] = []
        for move in args[searchmoves+1:]:
            options['searchmoves'].append(move)

    return options

class Engine:
    """
    Base class for an engine. this should be subclassed or use the friendler
    from_function method.

    If you subclass this you will need to implement
    def move(board: chess.Board, options: dict)

    Optional implementations
    uci()
    isready()
    ucinewgame()
    quit()
    stop()
    ponder()
    ponderhit()
    """

    def print(self, *args, **kwargs):
        """
        Wrapper around print so that it can be tested.
        Also always flushes.
        """
        print(*args, **kwargs, flush = True)

    def __init__(self, stdin = sys.stdin):
        self.board = chess.Board()
        self.stdin = stdin
        self.UCI_CMDS = {
            'uci': self.uci,
            'isready': self.isready,
            'ucinewgame': self.ucinewgame,
            'position': self.position,
            'go': self.go,
            'quit': self.quit,
            'stop': self.stop,
            'ponderhit': self.ponderhit,
        }

    def serve_uci(self):
        for line in self.stdin:
            args = line.split()
            if len(args) < 1:
                self.print(f'Unknown command: {line}')
                continue
            if self.UCI_CMDS.get(args[0]):
                self.UCI_CMDS[args[0]](args[1:] if len(args) > 1 else [])
            else:
                self.print(f'Unknown command: {line}')

    @classmethod
    def serve_lichess(cls, token: str, concurrency = 1,
        time_controls = ['bullet', 'blitz', 'rapid'], modes = ['casual'],
        verbose = False, upgrade = False, url = 'https://lichess.org/'):
        """
        Serve an engine to lichess.

        Args
        token: Your lichess OAUTH2 API token.
        concurrency: How meny games to play in parallel
        time_controls: A list of time controls to play, valid entries are
            ultraBullet
            bullet
            blitz
            rapid
            classical
            correspondence

        modes: What modes to play, valid entires are
            casual
            rated

        verbose: Verbose logging
        upgrade: Upgrade to a bot account if needed
        url: Lichess base url
        """

        from . import lichess_bot as li
        li.serve_lichess(token, cls, concurrency = concurrency,
                         time_controls = time_controls, modes = modes,
                         verbose = verbose, upgrade = upgrade, url = url)

    def move(self, board: chess.Board, options: dict) -> chess.Move:
        """
        Make a move. options will be a dict containing the options passed to the
        UCI 'go' command.

        Valid options include
        * wtime: int
            white has x msec left on the clock
        * btime: int
            black has x msec left on the clock
        * winc: int
            white increment per move in mseconds if x > 0
        * binc: int
            black increment per move in mseconds if x > 0
        * movestogo: int
            there are x moves to the next time control,
            this will only be sent if x > 0,
            if you don't get this and get the wtime and btime it's sudden death
        * depth: int
            search x plies only.
        * nodes
            search x nodes only,
        * mate
            search for a mate in x moves
        * movetime
            search exactly x mseconds

        For a full list view http://wbec-ridderkerk.nl/html/UCIProtocol.html
        """
        raise NotImplementedError

    def go(self, args):
        # We do nothing if the game is over.
        if not self.board.is_game_over():
            options = go(args)
            move = self.move(self.board, options)
            self.print(f'bestmove {move}')

    def position(self, args):
        self.board = position(args)

    def quit(self, _):
        """
        Quit the program as soon as possible.
        """
        exit()

    def stop(self, _ = []): # default [] because stop takes no args
        """
        stop calculating as soon as possible,
	don't forget the "bestmove" and possibly the "ponder" token when finishing the search
        """
        pass

    def uci(self, _):
        """
        Initialize the engine to use the UCI protocol.
        By default this just prints 'uciok', but you can overwrite this method
        to print information about you as well
        eg

        def uci(self, _):
            self.print('id name epic bot')
            self.print('id author epic coder')

            self.print('uciok')
        """
        self.print('uciok')

    def isready(self, _):
        """
        Called by the GUI to check if the engine is ready for commands.
        """
        self.print('readyok')

    def ucinewgame(self, _):
        """
        Called when a new game is started
        """
        pass

    def ponderhit(self, _):
        """
        Called when the move we pondered was played.
        """
        pass
