import unittest
import ezengine as ez

class TestPosition(unittest.TestCase):
    def test_fen(self):
        fen = 'rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq - 0 1'
        board = ez.position(('fen ' + fen).split(' '))
        self.assertEqual(board.fen(), fen)

    def test_startpos(self):
        fen = 'rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq - 0 1'
        board = ez.position('startpos moves e2e4'.split(' '))
        self.assertEqual(board.fen(), fen)

    def test_moves_and_fen(self):
        want_fen = 'rnbqk2r/ppppppbp/5np1/8/8/5NPP/PPPPPPB1/RNBQK2R b KQkq - 0 4'
        board = ez.position('fen rnbqk2r/ppppppbp/5np1/8/8/5NP1/PPPPPPBP/RNBQK2R w KQkq - 2 4 moves h2h3'.split(' '))
        self.assertEqual(board.fen(), want_fen)

class TestServeUCI(unittest.TestCase):
    # TODO
    pass

class TestEngine(ez.Engine):
    def move(self, board, options):
        if board.is_game_over():
            raise ValueError('move should never be called when the game is over')

        raise ValueError('in tests move() is never called with an ongoing game')

class TestGo(unittest.TestCase):
    def test_simple(self):
        options = ez.go('wtime 600 btime 600'.split(' '))
        self.assertEqual(options, {
            'wtime': 600,
            'btime': 600,
        })

    def test_complex(self):
        options = ez.go('infinite ponder searchmoves e2e4 d2d4'.split(' '))
        self.assertEqual(options, {
            'infinite': True,
            'searchmoves': ['e2e4', 'd2d4'],
            'ponder': True,
        })

    def test_game_over(self):
        engine = TestEngine()
        engine.position('fen rnbqkbnr/pp3Qpp/2pp4/4p3/2B1P3/8/PPPP1PPP/RNB1K1NR b KQkq - 0 4'.split())

        engine.go('')

if __name__ == '__main__':
    unittest.main()
