# ezengine

Provides a simple way to get started writing UCI enabled chess engines.
(mostly for lichess bots)

## Example

Here is a simple UCI engine that just plays random moves.
```py
import ezengine as ez
import chess
import random

class Engine(ez.Engine):
    def move(self, board: chess.Board, options: dict) -> chess.Move:
        print(f'make_move options: {options}')
        print(board)
        return random.choice(list(board.legal_moves))

# serve_lichess is a classmethod
if __name__ == '__main__':
  Engine.serve_lichess('xxxxxxxxxxxxxxxx', verbose=True)

# serve the engine over the uci protocol
engine = Engine()
engine.serve_uci()

# serve to lichess (replace xxx with your token)
# engine.serve_lichess('xxxxxxxxxxxxxxxx')
```
Note that this is not fully UCI compliant, it ignores options (ponder infinity etc)
